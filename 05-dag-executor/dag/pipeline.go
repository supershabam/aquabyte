package dag

import (
	"context"

	"golang.org/x/sync/errgroup"
)

type Pipeline struct {
	Source     Sourcer
	Transforms []Transformer
	Sink       Sinker
}

func (p *Pipeline) Run(ctx context.Context) error {
	eg, ctx := errgroup.WithContext(ctx)
	ch, wait := p.Source.Source(ctx)
	eg.Go(wait)
	for _, tr := range p.Transforms {
		ch, wait = tr.Transform(ctx, ch)
		eg.Go(wait)
	}
	eg.Go(func() error {
		return p.Sink.Sink(ctx, ch)
	})
	return eg.Wait()
}
