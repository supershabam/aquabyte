# problem

Storage costs of imagry will scale linearly with each new farm.
The ability to compress imagry in a way that still works for machine learning could increase margins per farm.

## context

Images are gathered from farm cameras at a rate of 1 frame per second from two cameras: left and right.
Each image is about 13MB in size.
Only half the day is usable (nighttime imagry is not valuable).
So, half day in seconds * 2 (left + right) * 13 MB = 43200 * 2 * 13 MB = 1123200 MB =~ 1.12 TiB.
Thus, each farm is gathering about 1.12 TiB of data per day.

The cost of google single-region object storage is $0.02 / GB / Month.
Let's assume we keep the images around for 7 days ~= 1/4 month.
Thus, each day's worth of images will cost 1.12 TiB * 1/4 * $0.02 / GB = 1.12e12 * 0.25 * 0.02 / 1e9 = $5.6.
So, the imagery costs are $5.6 / farm / day

The price here of $5.6/farm/day seems justifyable.
For a month, that means ~$150/farm.
It's probably not worth doing this idea until AquaByte scales to hundreds of farms OR if we need to retain data for longer.
It might be a good idea to archive farm images in case we need to audit past years/months on new algorithms.

## hypothesis

Instead of storing data from farms as images, let's store batches of images as a video.
We can use a lossless compression algorithm, and since much of the imagry will be the same, we can gain compression by having multiple frames in one video.

### quick research

https://en.wikipedia.org/wiki/Lossless_compression

https://en.wikipedia.org/wiki/List_of_codecs#Lossless_video_compression

https://en.wikipedia.org/wiki/FFV1

https://trac.ffmpeg.org/wiki/Encode/FFV1

## experiment

Let's use FFV1 fia ffmpeg library for this experiment.

Since I don't have fish source imagry, let's get some.
Flickr is a resource.
Let's find a high quality underwater video and convert it to a series of images.

https://www.flickr.com/photos/52376189@N05/9671980576/

Doh, I don't have ffmpeg on my mac; I'll just use docker.

`docker pull jrottenberg/ffmpeg`

I've downloaded the video to my working directory and named it "video.mp4"

`docker run -v $(pwd):/tmp jrottenberg/ffmpeg -i /tmp/video.mp4 /tmp/images/image-%03d.png`

Why did I choose png and not jpeg?
I don't want to bias the approach by storing the frames as lossy compressed jpg, rather I want the pure rendered pixels (even though my source is mp4 which is compressed video).
This hypothesis really needs to be tested on real data for any validity, but I'd like to get _some_ idea of the compression numbers.

Note: this is taking a while.
I didn't time the command, but it's been at least a minute.
It is doint 5.7 fps.
I am not using hardware accelerated ffmpeg.

```
du -sh *
  3.4G	images
  31M	video.mp4
```

So, my images are 3.4 GB in total, but the source video is 31M.
Now, the source video was recorded in a lossy compression, so let's create a video out of the images using the FFV1 encoding.

Taking a break... It's been 40 minutes.
Back.
Timer reset!

`docker run -v $(pwd):/tmp jrottenberg/ffmpeg -i /tmp/images/image-%03d.png -c:v ffv1 -qscale:v 0 /tmp/video.avi`

This will take a similar amount of time: 5.7 fps.

Question: can we verify that a frame extracted from our video.avi we created will exactly match the input frame image-001.png?
This is important for machine learning that the raw data is not altered.

```
du -sh *
  3.4G	images
  1.1G	video.avi
   31M	video.mp4
```

So, we were able to get a compression resulting in about 1/3 data size.

Let's address the question I wrote down while I was waiting: can we extract frame 1 from the video and have it exactly match?

Also, my mac can't open that video.
It may not support FFV1 directly; perhaps VLC will play it.

I don't know how to extract a single frame.
https://video.stackexchange.com/questions/19873/extract-specific-video-frames
Let's try this.
`ffmpeg -i in.mp4 -vf select='between(n,x,y)' -vsync 0 frames%d.png`

Having trouble with this selection method; it is complaining about my arguments to "between."
Let's just try a different way really quickly to extract a single frame: by limiting the output to a single frame.

`docker run -v $(pwd):/tmp jrottenberg/ffmpeg -i /tmp/video.avi -vframes 1 /tmp/frame.png`

Success!

```
diff frame.png images/image-001.png
# note: no error

# now, let's compare against image-002 and expect an error
diff frame.png images/image-002.png
Binary files frame.png and images/image-002.png differ
```

## conclusion

Storing images as video is useful for archival purposes, but not for streaming pipeline.

The ability to minimize the total data size of image frames by packing them into lossless video is an approach to consider for archiving footage.
There is processing time involved to convert between video and image, but we do not need to extract the entire video to get to
just a subset of the frames that we care about.
As long as the storage of raw images are purged after a window of time (7 days), the storage costs are not a huge cost ($150/month/farm).
However, it may be useful to store farm images indefinitely so that past years or months of captured footage may be reprocessed with new algorithms to validate/train on actual yield results from that season/year.

More research should be done on the lossless compression format.
It might be beneficial to store both left and right footage in ONE video as the similarities between both left and right images could lead to highly compressable data.

Let's consider the archival costs of a years worth of farm data.

Inputs:
- 1TiB / day / farm of generated data
- $0.02 / GB / month of storage costs
- 0.33 compression ratio

Cost of 365 days of data for one farm stored for one year:  
= 12 months * $0.02 / GB / month * 1 TiB / day / farm * 365 days  
= $0.24 / GB * 365 TiB / farm  
= $87,600 / farm  

Cost of 365 days of data for one farm stored for one year WITH VIDEO COMPRESSION  
= $87,600 / farm * 0.33  
= $28,908 / farm  

Note: the compression ratio used of 0.33 is based on a single result of a random video I found on Flickr.
This experiment must be run on real data to get a better idea bout the cost savings this approach may actually have.

When considering running this on real data, consider that video may compress better when more total similar frames are included, e.g., a video of 10 frames total will not compress as well as a video of 2000 frames. Tuning the target number of frames should be done and balance the achieved compression ratio, the amount of time to create the video, and the total size of the resulting video.

## time spent

1 hour 15 minutes
