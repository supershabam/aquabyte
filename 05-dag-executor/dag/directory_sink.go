package dag

import (
	"context"
	"fmt"
	"io"
	"os"
	"path/filepath"

	"go.uber.org/zap"
)

type DirectorySink struct {
	Dir string
}

func (ds *DirectorySink) Sink(ctx context.Context, in <-chan Datum) error {
	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case d, active := <-in:
			if !active {
				return nil
			}
			zap.L().Info("processing datum...")
			r, err := d.Content()
			if err != nil {
				return err
			}
			zap.L().Info("got content")
			for {
				zap.L().Info("reading header...")
				fmt.Printf("datum = %T\n", d)
				fmt.Printf("%+v\n", d)
				hdr, err := r.Next()
				if err == io.EOF {
					zap.L().Info("done reading datum")
					break
				}
				fmt.Printf("header=%+v\n", hdr)
				if err != nil {
					return err
				}
				path := filepath.Join(ds.Dir, hdr.Name)
				zap.L().With(zap.String("path", path)).Info("creating file")
				// TODO mkdir -p the path it's trying to write
				// not as important for a GCS sink
				f, err := os.Create(path)
				if err != nil {
					return nil
				}
				zap.L().With(zap.String("path", path)).Info("writing")
				_, err = io.Copy(f, r)
				if err != nil {
					return err
				}
				err = f.Close()
				if err != nil {
					return err
				}
			}
		}
	}
}
