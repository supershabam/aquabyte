package dag

import (
	"archive/tar"
	"bytes"
	"context"
	"encoding/json"
	"io"
	"path/filepath"
)

type FilterExtension struct {
	Extension string
}

func (fe *FilterExtension) ID() string {
	b, err := json.Marshal(map[string]interface{}{
		"function": "FilterExtension",
		"version":  "v1.0.0",
		"config": map[string]interface{}{
			"Extension": fe.Extension,
		},
	})
	if err != nil {
		panic(err)
	}
	return string(b)
}

func (fe *FilterExtension) Transform(ctx context.Context, in Datum) (Datum, error) {
	r, err := in.Content()
	if err != nil {
		return nil, err
	}
	buf := bytes.NewBuffer(make([]byte, 0))
	w := tar.NewWriter(buf)
	for {
		hdr, err := r.Next()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}
		if filepath.Ext(hdr.Name) != fe.Extension {
			continue
		}
		err = w.WriteHeader(hdr)
		if err != nil {
			return nil, err
		}
		_, err = io.Copy(w, r)
		if err != nil {
			return nil, err
		}
	}
	err = w.Close()
	if err != nil {
		return nil, err
	}
	return &InmemoryDatum{
		Bytes: buf.Bytes(),
	}, nil
}
