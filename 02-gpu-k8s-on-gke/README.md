# gpu k8s on gke

## Purpose

Show off the ability to utilize google cloud for GPU instances abstracted by managed kubernetes and how quickly we can get started with this pattern.
Show off some of the difficulties of GPU scheduling and why its harder than just running a program using the GPU.

## journal

I spun up a GKE cluster with the default kubernetes version of 1.9, which was a mistake for getting things done quickly.
Version 1.9 doesn't expose the new nvidia/gpu allocatable resource by default, so I'm tearing down the cluster and spinning up v1.10.
I could have messed around with settings to expose the alpha feature in v1.9, but much easier to just us v1.10.

We need to install a daemon set to expose the GPU resource before it can become available.

`kubectl apply -f https://raw.githubusercontent.com/GoogleCloudPlatform/container-engine-accelerators/stable/nvidia-driver-installer/cos/daemonset-preloaded.yaml`

This takes a while to install...
Waiting for 5 minutes now.

I can see the GPU resource.

```
kubectl describe nodes

Allocatable:
 cpu:                940m
 ephemeral-storage:  47093746742
 hugepages-2Mi:      0
 memory:             2702204Ki
 nvidia.com/gpu:     1
 pods:               110
```

Let's schedule a program.

`kubectl apply -f cuda-print-smi-manifest.yaml`

```
kubectl get pods
NAME       READY     STATUS    RESTARTS   AGE
smi-test   1/1       Running   0          4s
```

Let's see the logs.

```
kubectl logs smi-test -f
Sat Aug  4 20:45:29 2018
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 390.46                 Driver Version: 390.46                    |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|===============================+======================+======================|
|   0  Tesla K80           Off  | 00000000:00:04.0 Off |                    0 |
| N/A   42C    P8    29W / 149W |      0MiB / 11441MiB |      0%      Default |
+-------------------------------+----------------------+----------------------+

+-----------------------------------------------------------------------------+
| Processes:                                                       GPU Memory |
|  GPU       PID   Type   Process name                             Usage      |
|=============================================================================|
|  No running processes found                                                 |
+-----------------------------------------------------------------------------+
```

Now that this pod is scheduled and consuming the one GPU resource, let's describe the node again.

## conclusion

Kubernetes is a fantastic, highly customizable, flexible, huge-amount-of-developer-hours platform that is maturing on running GPU workloads.
Its a smart bet to bet on kubernetes, and google kubernetes engine makes is very easy to run a basic good-enough kubernetes cluster.
Google's stackdriver has gotten wildly better over the last 6 months and integrates nicely with GKE with sane defaults, which means we get to spend less time figuring out how to make our workloads/pods/services observable/alertable and more time focusing on our problem at hand: inferring data from fish images.

Though the ecosystem for running machine learning jobs on kubernetes is still new, there's a huge amount of effort being done to make it work.
We will have to deal with rough edges for a while, but at least the core infrastructure of making GPU's schedulable in kubernetes is maturing.

There are exciting things happening in machine learning pipeline software.
https://github.com/kubeflow/kubeflow is an exciting project, but still very alpha.
At a minimum, we can run our current beanstalk type flow on kubernetes by deploying those containers as kubernetes pods with exposed service ingresses.
By exposing the same functionality on kubernetes, we get to also look to the future of how we might better execute these pipeline DAGs with more expressive and automatic software to describe our pipelines which are built around kubernetes primitives.

Time spent: [1 hour 30 minutes]

Follow along with a screencast here: https://drive.google.com/file/d/1m78ZldWreGLPZRjNNN8XbOFDSGTuzpni/view?usp=sharing
