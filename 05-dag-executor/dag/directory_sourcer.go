package dag

import (
	"archive/tar"
	"bytes"
	"context"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"

	"go.uber.org/zap"

	"golang.org/x/sync/errgroup"
)

type DirectorySourcer struct {
	Dir string
}

func (ds *DirectorySourcer) Source(ctx context.Context) (<-chan Datum, WaitFunc) {
	out := make(chan Datum)
	eg, ctx := errgroup.WithContext(ctx)
	eg.Go(func() error {
		defer close(out)
		err := filepath.Walk(ds.Dir, func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if info.IsDir() {
				return nil
			}
			d, err := ds.load(path)
			if err != nil {
				return err
			}
			select {
			case <-ctx.Done():
				return ctx.Err()
			case out <- d:
				return nil
			}
		})
		if err != nil {
			return err
		}
		return nil
	})
	return out, eg.Wait
}

func (ds *DirectorySourcer) load(path string) (Datum, error) {
	if filepath.Ext(path) == ".tar" {
		return ds.loadTar(path)
	}
	return ds.loadFileAsTar(path)
}

func (ds *DirectorySourcer) loadTar(path string) (Datum, error) {
	zap.L().With(
		zap.String("path", path),
	).Info("loadTar")
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	return &InmemoryDatum{
		Bytes: b,
	}, nil
}

func (ds *DirectorySourcer) loadFileAsTar(path string) (Datum, error) {
	zap.L().With(
		zap.String("path", path),
	).Info("loadFileAsTar")
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	buf := bytes.NewBuffer(make([]byte, 0))
	w := tar.NewWriter(buf)
	err = w.WriteHeader(&tar.Header{
		Name: path,
		Mode: 0644,
		Size: int64(len(b)),
	})
	if err != nil {
		return nil, err
	}
	_, err = io.Copy(w, bytes.NewReader(b))
	if err != nil {
		return nil, err
	}
	err = w.Close()
	if err != nil {
		return nil, err
	}
	return &InmemoryDatum{
		Bytes: buf.Bytes(),
	}, nil
}
