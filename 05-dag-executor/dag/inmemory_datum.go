package dag

import (
	"archive/tar"
	"bytes"
	"crypto/sha256"
)

type InmemoryDatum struct {
	Bytes []byte
}

func (id *InmemoryDatum) Sha256() ([]byte, error) {
	h := sha256.New()
	h.Write(id.Bytes)
	return h.Sum(nil), nil
}

func (id *InmemoryDatum) Content() (*tar.Reader, error) {
	return tar.NewReader(bytes.NewReader(id.Bytes)), nil
}
