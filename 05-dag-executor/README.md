# dag executor

## purpose

I want to show off some ideas from first principles of operating data pipelines.
I find that "push" pipelines are more brittle than "pull" pipelines.
"Pull" pipelines also are able to execute lazily: don't do anything until somebody downstream cares for the data you can generate.
It's also important to note that often pipelines branch off to many different outputs, but have common inputs.
So, if we want to make a "pull" pipeline that is efficient, we need to memoize the data.
We also need to handle the ability to filter out data and the ability for a single input to generate multiple outputs.

## journal

Let's iterate on a fake image pipeline just in a single golang program and then show off how this might be abstracted away to containerized execution later.

Used some ideas of golang pipelines to set up an executable chain.
I've been doing work on this type of problem for a while.
Take a look at https://github.com/supershabam/pipeliner for my first attempt at a highly concurrent data pipeline.
I was trying to make a type-safe pipeline with pipeliner.
From this experiment, I find that flatMap type transforms are the most complicated, but handle most every possible use case (filter + fanout).

I need to work on memoization and also how to represent datum type in a better way.
I'm going to overload the `archive/tar` golang type as a tar file can provide the interface I need: hashable and may contain zero-many elements.
Additionally, for large large payloads, the single tar file can be split into parts if necessary, e.g., file.1.tar file.2.tar.

Taking a break.
This has been 40 minutes.

Alright, we have memoized results but the memoization is built into the transform directly.
The pipeline executor should be able to memoize results, but first we need to work on the `datum` abstraction.
The datum must be able to return the content to the caller so that the pipeliner can memoize between stages.

Realizing that I've made some mistakes on the transformer interface.
A filter shouldn't drop results from the channel; rather, a filter should return a tar with zero files in it.
It's important to memoize a result that ends up filtering out all the data.

I've manually tested the end-to-end output.
I've got a tar that has a reversed main.go in it and it gets memoized so it's not computed twice, even though the pipeline is computed twice.

Let's clean things up for a minute.
This main has gotten rather large.

Done.
It has now been nearly two hours.

Let's create a transformer that has some configurable arguments and figure out how we're going to memoize correctly to handle different args.

Running into a problem where my final DirectorySink is blocking on reading a Datum.
Am I locking a mutex somewhere?

Ah!
My `file_datum.go` is becoming recursive as it expects a non-tar origin file and expresses it as a datum (tar) automatically.

Alright, I have memoizable pipelines.
Let's create that PureTransform that takes some arguments...
Then, let's make a memoizer that talks to google cloud storage.

I'm on my last hour.
I need to figure out how to best spend the time.

Let's show of a tricky thing to get right in pipelines: fetching some new data and fanout.
So, let's add another transform that exists after the reverse transform that re-fetches the original golang file and produces an archive that has both the reversed file and the original (even though the pipeline has already dropped the original from its payload forward propagating).

## conclusion

Much more work is needed to make this a distributed system, but the core of expressing a pipeline and minimizing work is in-place.

I find that pipelines that rely on watching for a directory/database and then pushing a change to a directory/database and hoping that the next step is online and available is brittle and hard to scale.
I've been recently very interested in the "lazy" pipeline that is a description of all the pure function chains to derive the end-result data combined with a source filter to limit how much data should go into the pipeline a very scalable and comprehensible solution.
So long as independent, parallel pipelines depend on the same intermediate derived data, the data is memoized and shared among these other pipelines, but no human needs to draw out the pipeline and manually think on how to best share resources.

I've never done the "tar file as a delivery mechanism" before.
I think I like it.
It is a bit of a pain in golang to read and write a tar file, but it's a very expressive primitive that allows for empty results and also merging together many artifacts into one easily-memoizable file.

I was over-ambitious on how far I could get.
I wanted to break up the hard-coded pipeline struct to become just a YAML definition of a series of containers to run.
The container sha256 would become the "function version" and the container name would be the function name.
Any environment variables passed to the container would be the arguments that get hashed to ensure that the container is treated as a pure function.

I wanted to create a google cloud storage memoizer and sink.
The memoizer should write the tar files to a memoization bucket that has a lifecycle retention set to something like 3 days.
The sink should be configurable on how long the results should last, but I'd imagine most sinks would be indefinite as this is the final derived data of a pipeline.

This type of container-as-a-function pipeline can be orchestrated on kubernetes fairly easily with a custom scheduler.
The first-approach-scheduler would just orchestrate running a single input file down the pipeline.
However, a more advanced scheduler would batch together inputs and also be GPU-aware and running functions/containers that do not require a GPU in smaller batches, but GPU-workloads in larger batches and with long-lived containers that don't operate on a single input/output but rather are spun up and operate on a directroy of inputs outputting a directly of outputs.
This allows the GPU-intensive workloads to load all the GPU weights once, run a bunch of data through the pre-loaded GPU, and then unallocate the GPU.

[ time spent 3.5 hours ]