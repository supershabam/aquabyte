# aquabyte

## purpose

I don't think I actually interviewed well with AquaByte.
I wasn't able to really show off what I can bring to the table.
I'd like to negotiate for a better offer, but not without first really showing off more of my skills.

So, I've decided to run a few timeboxed experiments over the weekend.
I hope this shows off my ability to solve some problems from a business standpoint, and also from first principles.
I'm granting you an open source MIT license on any code and my intention that any good/bad ideas found herein are provided royalty free even if we don't end up working together.

## experiments

* [ 2 hours ] lossless video encoding as payload mechanism instead of frame images
* [ 2 hours ] standing up a gpu k8s cluster on google and running basic GPU program
* [ 2 hours ] standing up k8s cluster with gpu access on local machine
* [ 2 hours | didn't finish ] running open faas on k8s cluster utilizing gpu
* [ 4 hours ] designing a DAG executor on k8s to filter on left images and occasionally pull right images

## other notable pre-existing

Now that I've been on-site and talked more through your problems, I've got some other open-source projects I'd also like to share.

* [reglaze](https://github.com/emitio/reglaze)

Reglaze is an attempt to bring the good ideas from rxjs, react-redux, and reselect into a single library.
It technically works, but it's much too complicated.
I'm pointing you to it because though I don't do frontend development full-time, I've always kept myself adjacently plugged into the community and can power through web development as needed.

I'd point you at https://github.com/hshoff/vx for any custom dashboarding you may want to do.
In my search for charting libraries for making very custom components (side project that does histogram visualization), I was using vx to render out svg with react.

* [Raspbian](https://github.com/supershabam/raspbian)

Raspbian is a few basic scripts to build a brand new .img file that I can load onto a raspberry pi to bootstrap a kubernetes cluster.
When I was learning kubernetes (and also travelling...) I bought 3 Intel NUCs and configured them for netbooting.
I'd then automate bootstraping these machines into a cluster via scripts I'd have on the raspberry pi.

I think this experience is interesting for talking about bootstrapping/maintaining servers in the field.

My mobile k8s cluster:
![mobile kis cluster](mobile-k8s-cluster.jpg)
