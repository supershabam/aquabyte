package main

import (
	"archive/tar"
	"bytes"
	"context"
	"encoding/json"
	"io"
	"io/ioutil"
	"path/filepath"

	"gitlab.com/supershabam/aquabyte/05-dag-executor/dag"
	"go.uber.org/zap"
)

type reverseContentTransformer struct {
}

func reverse(s string) string {
	r := []rune(s)
	for i, j := 0, len(r)-1; i < len(r)/2; i, j = i+1, j-1 {
		r[i], r[j] = r[j], r[i]
	}
	return string(r)
}

func (rct *reverseContentTransformer) ID() string {
	info := map[string]interface{}{
		"function":      "reverseContentTransformer",
		"version":       "v1.0.0",
		"configuration": map[string]interface{}{},
	}
	b, err := json.Marshal(info)
	if err != nil {
		panic(err)
	}
	return string(b)
}

func (rct *reverseContentTransformer) Transform(ctx context.Context, d dag.Datum) (dag.Datum, error) {
	r, err := d.Content()
	if err != nil {
		return nil, err
	}
	buf := bytes.NewBuffer(make([]byte, 0))
	w := tar.NewWriter(buf)
	for {
		hdr, err := r.Next()
		if err == io.EOF {
			break // end of archive
		}
		if err != nil {
			return nil, err
		}
		err = w.WriteHeader(hdr)
		if err != nil {
			return nil, err
		}
		b, err := ioutil.ReadAll(r)
		if err != nil {
			return nil, err
		}
		next := []byte(reverse(string(b)))
		if _, err := io.Copy(w, bytes.NewReader(next)); err != nil {
			return nil, err
		}
	}
	err = w.Close()
	if err != nil {
		return nil, err
	}
	return &dag.InmemoryDatum{
		Bytes: buf.Bytes(),
	}, nil
}

type fetchOriginalAndMerge struct {
	Dir string
}

func (foam *fetchOriginalAndMerge) ID() string {
	b, err := json.Marshal(map[string]interface{}{
		"function": "fetchOriginalAndMerge",
		"version":  "v1.0.0",
		"config": map[string]interface{}{
			"Dir": foam.Dir,
		},
	})
	if err != nil {
		panic(err)
	}
	return string(b)
}

func (foam *fetchOriginalAndMerge) Transform(ctx context.Context, in dag.Datum) (dag.Datum, error) {
	buf := bytes.NewBuffer(make([]byte, 0))
	w := tar.NewWriter(buf)
	r, err := in.Content()
	if err != nil {
		return nil, err
	}
	for {
		hdr, err := r.Next()
		if err == io.EOF {
			break
		}
		// write the incoming payload file into the outgoing payload
		err = w.WriteHeader(hdr)
		if err != nil {
			return nil, err
		}
		_, err = io.Copy(w, r)
		if err != nil {
			return nil, err
		}
		// now fetch the original source and also add to the outgoing payload
		b, err := ioutil.ReadFile(filepath.Join(foam.Dir, hdr.Name))
		if err != nil {
			return nil, err
		}
		hdr = &tar.Header{
			Name: hdr.Name + ".original",
			Mode: 0644,
			Size: int64(len(b)),
		}
		err = w.WriteHeader(hdr)
		if err != nil {
			return nil, err
		}
		_, err = io.Copy(w, bytes.NewReader(b))
		if err != nil {
			return nil, err
		}
	}
	err = w.Close()
	if err != nil {
		return nil, err
	}
	return &dag.InmemoryDatum{
		Bytes: buf.Bytes(),
	}, nil
}

func run(ctx context.Context) error {
	const memoDir = "./memo"
	// p := &dag.Pipeline{
	// 	Source: &dag.DirectorySourcer{
	// 		Dir: "./",
	// 	},
	// 	Transforms: []dag.Transformer{
	// 		&dag.MemoizingTransformer{
	// 			MemoizationDir: memoDir,
	// 			Transformer:    &reverseContentTransformer{},
	// 		},
	// 	},
	// 	Sink: &dag.StdoutSink{},
	// }
	// err := p.Run(ctx)
	// if err != nil {
	// 	return err
	// }
	// // run the same pipeline again... we expect the pipeline to use the memoized data
	// err = p.Run(ctx)
	// if err != nil {
	// 	return err
	// }

	// create a new pipeline that has same source and transform, but sink into output dir
	p2 := &dag.Pipeline{
		Source: &dag.DirectorySourcer{
			Dir: "./dag",
		},
		Transforms: []dag.Transformer{
			&dag.MemoizingTransformer{
				MemoizationDir: memoDir,
				Transformer: &dag.FilterExtension{
					Extension: ".go",
				},
			},
			&dag.MemoizingTransformer{
				MemoizationDir: memoDir,
				Transformer:    &reverseContentTransformer{},
			},
			&dag.MemoizingTransformer{
				MemoizationDir: memoDir,
				Transformer: &fetchOriginalAndMerge{
					Dir: "./",
				},
			},
		},
		Sink: &dag.DirectorySink{
			Dir: "./out",
		},
	}
	err := p2.Run(ctx)
	if err != nil {
		return err
	}
	return nil
}

func main() {
	l, _ := zap.NewDevelopment()
	zap.ReplaceGlobals(l)
	ctx := context.Background()
	err := run(ctx)
	if err != nil {
		zap.L().With(
			zap.Error(err),
		).Fatal("running pipeline")
	}
}
