package dag

import (
	"archive/tar"
	"io/ioutil"
	"sync"
)

type fileDatum struct {
	Path string

	inmem *InmemoryDatum
	m     sync.Mutex
}

func (fd *fileDatum) Sha256() ([]byte, error) {
	err := fd.load()
	if err != nil {
		return nil, err
	}
	return fd.inmem.Sha256()
}

func (fd *fileDatum) Content() (*tar.Reader, error) {
	err := fd.load()
	if err != nil {
		return nil, err
	}
	return fd.inmem.Content()
}

func (fd *fileDatum) load() error {
	fd.m.Lock()
	defer fd.m.Unlock()
	if fd.inmem != nil {
		return nil
	}
	b, err := ioutil.ReadFile(fd.Path)
	if err != nil {
		return err
	}
	fd.inmem = &InmemoryDatum{
		Bytes: b,
	}
	return nil
}
