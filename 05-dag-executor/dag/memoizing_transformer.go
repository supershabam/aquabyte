package dag

import (
	"archive/tar"
	"bytes"
	"context"
	"crypto/sha256"
	"encoding/base64"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"

	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
)

type PureTransformer interface {
	// ID is a human friendly string that must uniquely describe the pure transform and its initialization arguments
	ID() string
	Transform(ctx context.Context, in Datum) (Datum, error)
}

type MemoizingTransformer struct {
	MemoizationDir string
	Transformer    PureTransformer
}

func (mt *MemoizingTransformer) Transform(ctx context.Context, in <-chan Datum) (<-chan Datum, WaitFunc) {
	out := make(chan Datum)
	eg, ctx := errgroup.WithContext(ctx)
	eg.Go(func() error {
		defer close(out)
		for {
			select {
			case <-ctx.Done():
				return ctx.Err()
			case d, active := <-in:
				if !active {
					return nil
				}
				path, err := mt.path(d)
				if err != nil {
					return err
				}
				exists, err := mt.exists(path)
				if err != nil {
					return err
				}
				zap.L().With(
					zap.String("path", path),
					zap.Bool("exists", exists),
				).Info("result of whether or not file exists")
				// if memoization exists, pass a lazy file Datum down the pipeline
				if exists {
					fd := &fileDatum{
						Path: path,
					}
					select {
					case <-ctx.Done():
						return ctx.Err()
					case out <- fd:
					}
					continue
				}
				// otherwise run transformFunc and save result before passing down the pipeline
				next, err := mt.Transformer.Transform(ctx, d)
				if err != nil {
					return err
				}
				err = mt.save(ctx, path, next)
				if err != nil {
					return err
				}
				select {
				case <-ctx.Done():
					return ctx.Err()
				case out <- next:
				}
			}
		}
	})
	return out, eg.Wait
}

func (mt *MemoizingTransformer) save(ctx context.Context, path string, d Datum) error {
	r, err := d.Content()
	if err != nil {
		return err
	}
	f, err := os.Create(path)
	if err != nil {
		return err
	}
	w := tar.NewWriter(f)
	for {
		hdr, err := r.Next()
		if err == io.EOF {
			break // end of archive
		}
		if err != nil {
			return err
		}
		b, err := ioutil.ReadAll(r)
		if err != nil {
			return err
		}
		zap.L().With(
			zap.String("name", hdr.Name),
			zap.Int("len", len(b)),
		).Info("writing header")
		err = w.WriteHeader(&tar.Header{
			Name: hdr.Name,
			Mode: 0644,
			Size: int64(len(b)),
		})
		if err != nil {
			return err
		}
		if _, err := io.Copy(w, bytes.NewReader(b)); err != nil {
			return err
		}
	}
	err = w.Close()
	if err != nil {
		return err
	}
	return nil
}

func (mt *MemoizingTransformer) path(d Datum) (string, error) {
	datumsha, err := d.Sha256()
	if err != nil {
		return "", err
	}
	h := sha256.New()
	h.Write([]byte(mt.Transformer.ID()))
	h.Write(datumsha)
	consistentID := base64.RawURLEncoding.EncodeToString(h.Sum(nil))
	path := filepath.Join(mt.MemoizationDir, consistentID+".tar")
	return path, nil
}

func (mt *MemoizingTransformer) exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err != nil && !os.IsNotExist(err) {
		return false, err
	}
	if err == nil {
		return true, nil
	}
	return false, nil
}
