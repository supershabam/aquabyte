# running open faas

## problem

For quick iteration on a ML model, it is convenient to have a HTTP service to send data to and get a result.
In kubernetes, it's not resource-smart to keep every version of every model online as a container with its own service definition.
Rather, it would be nice to expose a single service that can specify a "function name" which is a ML model pushed up to a docker registry as a container.
This way, the function/container is available from a single HTTP service but also scales down to 0 when it is not in-use.

## conclusion

I ran out of time this weekend to investigate this fully.
I was happier to spend more hours on the dag executor than merely try standing up some existing software to get a more in-depth yet still quick opinion on it.

I was going to look at a few projects.

https://github.com/fission/fission/issues/762

https://github.com/openfaas/faas-netes/pull/264

https://gitlab.com/meltano/meltano

https://github.com/kubeflow/kubeflow

[ time spent : 15 minutes ]