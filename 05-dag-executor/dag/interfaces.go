package dag

import (
	"archive/tar"
	"context"
)

type WaitFunc func() error

type Datum interface {
	Content() (*tar.Reader, error)
	Sha256() ([]byte, error)
}

type Sourcer interface {
	Source(ctx context.Context) (<-chan Datum, WaitFunc)
}

type Transformer interface {
	Transform(ctx context.Context, in <-chan Datum) (<-chan Datum, WaitFunc)
}

type Sinker interface {
	Sink(ctx context.Context, in <-chan Datum) error
}
