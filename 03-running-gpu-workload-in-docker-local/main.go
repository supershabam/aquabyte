package main

import (
	"bytes"
	"context"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os/exec"
	"path/filepath"
	"sync"
)

type yolo struct {
	m sync.Mutex
}

func newYolo() *yolo {
	return &yolo{}
}

// server is a very dumb http server, it expects a jpeg body payload and
// returns a jpeg body payload (or http 500 with error text)
type server struct {
	yolo *yolo
}

func newServer(y *yolo) *server {
	return &server{y}
}

func (s *server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	onErr := func(err error) {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	in, err := ioutil.ReadAll(r.Body)
	if err != nil {
		onErr(err)
		return
	}
	defer r.Body.Close()
	out, err := s.yolo.predict(r.Context(), in)
	if err != nil {
		onErr(err)
		return
	}
	w.Header().Set("content-type", "image/jpeg")
	io.Copy(w, bytes.NewReader(out))
}

// predict wraps the execution of `darknet detect ...` and works around some quirks of how the command executes
// where this really wasn't meant to be an API but we want it to act like one.
func (y *yolo) predict(ctx context.Context, in []byte) ([]byte, error) {
	// run prediction behind a mutex as we have a single working directory that we will stomp on files
	// if we try to run concurrently.
	y.m.Lock()
	defer y.m.Unlock()
	f, err := ioutil.TempFile("", "darknet")
	if err != nil {
		return nil, err
	}
	_, err = io.Copy(f, bytes.NewReader(in))
	if err != nil {
		return nil, err
	}
	path := f.Name()
	f.Close()
	cmd := exec.CommandContext(ctx, "/usr/local/bin/darknet", "detect", "cfg/yolov3.cfg", "yolov3.weights", path)
	cmd.Dir = "/var/lib/darknet"
	// This opens the display!
	// How do I supress this? Because it blocks the process going forward!
	// This may not be a problem inside of a docker container as it won't be able to attach to the display...
	out, err := cmd.CombinedOutput()
	log.Print(string(out))
	// don't return error here
	// if err != nil {
	// 	return nil, err
	// }
	b, err := ioutil.ReadFile(filepath.Join("/var/lib/darknet", "predictions.jpg"))
	if err != nil {
		return nil, err
	}
	return b, nil
}

func main() {
	ctx := context.Background()
	err := run(ctx)
	if err != nil {
		panic(err)
	}
}

func run(ctx context.Context) error {
	y := newYolo()
	s := newServer(y)
	svr := &http.Server{
		Handler: s,
		Addr:    ":8080",
	}
	return svr.ListenAndServe()
}
