package dag

import (
	"context"
	"fmt"
)

type StdoutSink struct{}

func (ss *StdoutSink) Sink(ctx context.Context, in <-chan Datum) error {
	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case d, active := <-in:
			if !active {
				return nil
			}
			fmt.Printf("%+v\n", d)
		}
	}
}
